//
//  AppDelegate.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/27/17.
//  Copyright � 2017 App Magic. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    static var appIsBackgrounded = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.backgroundColor = UIColor.white
        window!.makeKeyAndVisible()
        
        GMSServices.provideAPIKey("Your key")
        
        let placeTypesListVC = PlaceTypesList_VC(nibName: "PlaceTypesList_VC", bundle: nil)
        let navController = UINavigationController(rootViewController: placeTypesListVC)
        window!.rootViewController = navController
        
        if UIFont(name: "Montserrat-Bold", size: 20) != nil
        {
            print("Font exists!")
        }
        else
        {
            print("Font does not exist")
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        AppDelegate.appIsBackgrounded = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        AppDelegate.appIsBackgrounded = false
    }
    
    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

