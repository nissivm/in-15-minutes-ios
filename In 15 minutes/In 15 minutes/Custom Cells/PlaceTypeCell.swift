//
//  PlaceTypeCell.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/10/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PlaceTypeCell: UICollectionViewCell
{
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeTypeName: UILabel!
}
