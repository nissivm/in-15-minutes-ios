//
//  BusinessInfoCell.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/23/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

protocol BusinessInfoCellDelegate: class
{
    func yelpLogoButtonTapped()
    func routeButtonTapped()
    func placePhoneButtonTapped()
    func websiteButtonTapped()
}

class BusinessInfoCell: UITableViewCell
{
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var yelpLogoButton: UIButton!
    
    @IBOutlet weak var blockOneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeNameLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var walkingManImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var routeButton: UIButton!
    
    @IBOutlet weak var walkingManImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var markerImageView: UIImageView!
    @IBOutlet weak var placeAddressLabel: UILabel!
    
    @IBOutlet weak var blockThreeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeAddressLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var placePhoneButton: UIButton!
    
    @IBOutlet weak var yelpBurstImageView: UIImageView!
    @IBOutlet weak var websiteButton: UIButton!
    
    weak var delegate: BusinessInfoCellDelegate?
    private var business: Business!
    private var multiplier: CGFloat = 1
    
    //-------------------------------------------------------------------------//
    // MARK: Setup cell content
    //-------------------------------------------------------------------------//
    
    func setupCellContent(business: Business)
    {
        self.business = business
        //multiplier = Auxiliar.retrieveMultiplier()
        
        
        
        let blockOneRemainingSpace = (120.0 * multiplier) - (25.0 * multiplier)
        let placeName = business.getPlaceName()
        let placeNameLabelH = placeName.labelHeightForSelf(maxWidth: Auxiliar.getMaxWidth(),
                                                            fontSize: 22.0 * multiplier,
                                                            fontName: "Montserrat-SemiBold")
        blockOneHeightConstraint.constant = blockOneRemainingSpace + placeNameLabelH + 1.0
        placeNameLabelHeightConstraint.constant = placeNameLabelH + 1.0
        placeNameLabel.text = placeName
        
        ratingImageView.image = Auxiliar.getRatingImage(rating: business.getPlaceRating())
        
        let priceRange = business.getPlacePriceLevel()
        
        if priceRange != ""
        {
            priceRangeLabel.text = priceRange
        }
        else
        {
            priceRangeLabel.textColor = UIColor.darkGray
            priceRangeLabel.alpha = 0.2
        }
        
        reviewCountLabel.text = Auxiliar.getReviewText(reviewCount: business.getPlaceReviewCount())
        
        let distanceInSeconds = business.getPlaceDistance()
        let distanceInMinutes = distanceInSeconds/60
        distanceLabel.text = "\(Int(ceil(distanceInMinutes))) min away"
        
        let blockThreeRemainingSpace = (45.0 * multiplier) - (18.0 * multiplier)
        let placeAddress = business.getPlaceAddr()
        let placeAddressLabelH = placeAddress.labelHeightForSelf(maxWidth: Auxiliar.getMaxWidth(), fontSize: 15.0 * multiplier)
        blockThreeHeightConstraint.constant = blockThreeRemainingSpace + placeAddressLabelH + 1.0
        placeAddressLabelHeightConstraint.constant = placeAddressLabelH + 1.0
        placeAddressLabel.text = placeAddress
        
        let placePhone = business.getPlacePhone()
        
        if placePhone != ""
        {
            placePhoneButton.setTitle(placePhone, for: UIControlState())
        }
        else
        {
            placePhoneButton.setTitle("No phone number", for: UIControlState())
            placePhoneButton.titleLabel!.font = UIFont.italicSystemFont(ofSize: 15.0 * multiplier)
        }
        
        if business.getPlaceUrl() == ""
        {
            websiteButton.setTitle("No place url", for: UIControlState())
            websiteButton.titleLabel!.font = UIFont.italicSystemFont(ofSize: 15.0 * multiplier)
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func yelpLogoButtonTapped(_ sender: UIButton)
    {
        delegate!.yelpLogoButtonTapped()
    }
    
    @IBAction func routeButtonTapped(_ sender: UIButton)
    {
        delegate!.routeButtonTapped()
    }
    
    @IBAction func placePhoneButtonTapped(_ sender: UIButton)
    {
        guard business.getPlacePhone() != "" else
        {
            return
        }
        
        delegate!.placePhoneButtonTapped()
    }
    
    @IBAction func websiteButtonTapped(_ sender: UIButton)
    {
        guard business.getPlaceUrl() != "" else
        {
            return
        }
        
        delegate!.websiteButtonTapped()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Ajust for bigger screens
    //-------------------------------------------------------------------------//
    
    private func adjustForBiggerScreens()
    {
        guard walkingManImageViewHeightConstraint.constant == 30 else
        {
            return
        }
        
        for constraint in placeNameLabel.constraints
        {
            constraint.constant *= multiplier
        }
        
        placeNameLabel.font = UIFont.init(name: "Montserrat-SemiBold",
                                          size: 22.0 * multiplier)
        
        for constraint in ratingImageView.constraints
        {
            constraint.constant *= multiplier
        }
        
        for constraint in priceRangeLabel.constraints
        {
            constraint.constant *= multiplier
        }
        
        priceRangeLabel.font = UIFont.boldSystemFont(ofSize: 20.0 * multiplier)
        
        for constraint in reviewCountLabel.constraints
        {
            constraint.constant *= multiplier
        }
        
        reviewCountLabel.font = UIFont.systemFont(ofSize: 15.0 * multiplier)
        
        for constraint in yelpLogoButton.constraints
        {
            constraint.constant *= multiplier
        }
        
        for constraint in walkingManImageView.constraints
        {
            constraint.constant *= multiplier
        }
        
        for constraint in distanceLabel.constraints
        {
            constraint.constant *= multiplier
        }
        
        distanceLabel.font = UIFont.boldSystemFont(ofSize: 16.0 * multiplier)
        
        for constraint in routeButton.constraints
        {
            if (constraint.firstAttribute == NSLayoutAttribute.bottom) ||
               (constraint.firstAttribute == NSLayoutAttribute.top) ||
               (constraint.firstAttribute == NSLayoutAttribute.trailing)
            {
                constraint.constant *= multiplier
            }
        }
        
        for constraint in markerImageView.constraints
        {
            if constraint.firstAttribute != NSLayoutAttribute.centerY
            {
                constraint.constant *= multiplier
            }
        }
        
        for constraint in placeAddressLabel.constraints
        {
            constraint.constant *= multiplier
        }
        
        placeAddressLabel.font = UIFont.systemFont(ofSize: 15.0 * multiplier)
        
        for constraint in phoneImageView.constraints
        {
            constraint.constant *= multiplier
        }
        
        for constraint in placePhoneButton.constraints
        {
            constraint.constant *= multiplier
        }
        
        placePhoneButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15.0 * multiplier)
        
        for constraint in yelpBurstImageView.constraints
        {
            constraint.constant *= multiplier
        }
        
        for constraint in websiteButton.constraints
        {
            constraint.constant *= multiplier
        }
        
        websiteButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15.0 * multiplier)
    }
}
