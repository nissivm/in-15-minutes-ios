//
//  UIColorExtension.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/14/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

extension UIColor
{
    // Font: http://crunchybagel.com/working-with-hex-colors-in-swift-3/
    // Ex.: let color = UIColor(hex: "ff0000")
    
    convenience init(hex: String)
    {
        var rgbValue: UInt64 = 0
        
        let scanner = Scanner(string: hex)
            scanner.scanLocation = 0
            scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
