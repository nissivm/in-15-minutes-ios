//
//  StringExtension.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/18/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

extension String
{
    //-------------------------------------------------------------------------//
    // MARK: Calculate UILabel dimensions using custom font
    //-------------------------------------------------------------------------//
    
    func labelHeightForSelf(maxWidth: CGFloat, fontSize: CGFloat, fontName: String) -> CGFloat
    {
        let labelFrame = CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude,
                                height: CGFloat.greatestFiniteMagnitude)
        
        let label = UILabel(frame: labelFrame)
            label.numberOfLines = 1
            label.lineBreakMode = .byWordWrapping
            label.font = UIFont(name: fontName, size: fontSize)
            label.text = self
            label.sizeToFit()
        
        let oneLineHeight = label.frame.size.height
        let numOfLines = ceil(label.frame.size.width/maxWidth)
        
        return oneLineHeight * numOfLines
    }
    
    func labelWidthForSelf(fontSize: CGFloat, fontName: String) -> CGFloat
    {
        let labelFrame = CGRect(x: 0, y: 0,
                                width: CGFloat.greatestFiniteMagnitude, height: 50.0)
        
        let label = UILabel(frame: labelFrame)
            label.numberOfLines = 1
            label.lineBreakMode = .byWordWrapping
            label.font = UIFont(name: fontName, size: fontSize)
            label.text = self
            label.sizeToFit()
        
        return label.frame.size.width
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Calculate UILabel dimensions using system font
    //-------------------------------------------------------------------------//
    
    func labelHeightForSelf(maxWidth: CGFloat, fontSize: CGFloat) -> CGFloat
    {
        let labelFrame = CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude,
                                height: CGFloat.greatestFiniteMagnitude)
        
        let label = UILabel(frame: labelFrame)
            label.numberOfLines = 1
            label.lineBreakMode = .byWordWrapping
            label.font = UIFont.systemFont(ofSize: fontSize)
            label.text = self
            label.sizeToFit()
        
        let oneLineHeight = label.frame.size.height
        let numOfLines = ceil(label.frame.size.width/maxWidth)
        
        return oneLineHeight * numOfLines
    }
    
    func labelWidthForSelf(fontSize: CGFloat) -> CGFloat
    {
        let labelFrame = CGRect(x: 0, y: 0,
                                width: CGFloat.greatestFiniteMagnitude, height: 50.0)
        
        let label = UILabel(frame: labelFrame)
            label.numberOfLines = 1
            label.lineBreakMode = .byWordWrapping
            label.font = UIFont.systemFont(ofSize: fontSize)
            label.text = self
            label.sizeToFit()
        
        return label.frame.size.width
    }
}
