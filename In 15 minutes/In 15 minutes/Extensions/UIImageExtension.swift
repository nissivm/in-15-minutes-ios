//
//  UIImageExtension.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/17/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

extension UIImage
{
    class func resizeImage(_ img: UIImage, desiredSize: CGSize) -> UIImage
    {
        if (img.size.width == desiredSize.width) && (img.size.height == desiredSize.height)
        {
            return img
        }
        
        var newHeight = desiredSize.height
        var newWidth = (img.size.width * newHeight)/img.size.height
        var posX: CGFloat = 0
        var posY: CGFloat = 0
        
        if newWidth > desiredSize.width
        {
            posX = (newWidth - desiredSize.width)/2
            posX *= -1
        }
        
        if newWidth < desiredSize.width
        {
            let oldWidth = newWidth
            let oldHeight = newHeight
            newWidth = desiredSize.width
            newHeight = (newWidth * oldHeight)/oldWidth
        }
        
        if newHeight > desiredSize.height
        {
            posY = (newHeight - desiredSize.height)/2
            posY *= -1
        }
        
        let imgContainer = UIImageView(frame: CGRect(x: posX, y: posY, width: newWidth, height: newHeight))
            imgContainer.image = img
        
        let container = UIView(frame: CGRect(x: 0, y: 0, width: desiredSize.width, height: desiredSize.height))
            container.addSubview(imgContainer)
        
        UIGraphicsBeginImageContextWithOptions(container.frame.size, false, 0)
        container.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImg!
    }
}
