//
//  StreetViewContainerView.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 10/16/15.
//  Copyright © 2015 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class StreetViewContainerView: UIViewController, GMSMapViewDelegate
{
    static var placeLat: Double = 0
    static var placeLong: Double = 0
    
    override func loadView()
    {
        let panoView = GMSPanoramaView(frame: CGRect.zero)
        self.view = panoView
        
        let coord = CLLocationCoordinate2DMake(StreetViewContainerView.placeLat,
                                               StreetViewContainerView.placeLong)
        
        panoView.moveNearCoordinate(coord)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
