//
//  SearchResults_VC.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/14/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class SearchResults_VC: UIViewController, GMSMapViewDelegate
{
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelContainer: UIView!
    @IBOutlet weak var placeNameLabel: UILabel!
    
    @IBOutlet weak var backButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelContainerWidthConstraint: NSLayoutConstraint!
    
    private var launching = true
    private var center = NotificationCenter.default
    private var timer: Timer!
    private var userMarker: GMSMarker!
    private var geocoder: CLGeocoder = CLGeocoder()
    private var selectedMarker: GMSMarker?
    private var infoWindow: UIView?
    private var selMarkerId = ""
    private var userAddress = ""
    
    var placeName = ""
    var placeImgName = ""
    var filteredBusinesses = [Business]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let sel = #selector(SearchResults_VC.screenRotated)
        let name = NSNotification.Name.UIDeviceOrientationDidChange
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        let camera = GMSCameraPosition.camera(withLatitude: Global.userLat,
                                              longitude: Global.userLong, zoom: 16.0)
        mapView.camera = camera
        mapView.delegate = self
        
        placeNameLabel.text = placeName
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
        if Device.IS_IPHONE_X
        {
            backButtonTopConstraint.constant *= 2
            labelContainerTopConstraint.constant *= 2
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let labelW = placeName.labelWidthForSelf(fontSize: 15.0,
                                                 fontName: "Montserrat-SemiBold")
        
        labelContainerWidthConstraint.constant = labelW + 2.0 + (5.0 * 2)
        
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                     selector: #selector(placeMarkers),
                                     userInfo: nil, repeats: false)
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    deinit
    {
        center.removeObserver(self)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Timer function
    //-------------------------------------------------------------------------//
    
    @objc func placeMarkers()
    {
        // Place user marker:
        
        userMarker = GMSMarker()
        userMarker.position = CLLocationCoordinate2D(latitude: Global.userLat, longitude: Global.userLong)
        userMarker.icon = GMSMarker.markerImage(with: UIColor(hex: Constants.appGreen))
        userMarker.appearAnimation = .pop
        userMarker.map = mapView
        
        // Place businesses markers:
        
        for business in filteredBusinesses
        {
            let bLat = business.getPlaceLatitude()
            let bLong = business.getPlaceLongitude()
            
            let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: bLat, longitude: bLong)
                marker.userData = business.getPlaceId()
                marker.icon = GMSMarker.markerImage(with: UIColor(hex: Constants.appBlue))
                marker.appearAnimation = .pop
                marker.map = mapView
        }
        
        selectedMarker = userMarker
        getUserAddress()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Notifications
    //-------------------------------------------------------------------------//
    
    @objc func screenRotated()
    {
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) ||
           UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
        {
            if selectedMarker != nil
            {
                insertInfoWindow()
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        navigationController!.popViewController(animated: true)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: GMSMapViewDelegate
    //-------------------------------------------------------------------------//
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        selectedMarker = marker
        insertInfoWindow()
        
        //return false // Default behaviour
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        selectedMarker = nil
        
        if infoWindow != nil
        {
            infoWindow!.removeFromSuperview()
            infoWindow = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView?
    {
        return UIView()
    }
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        if (selectedMarker != nil) && (infoWindow != nil)
        {
            let projection = mapView.projection.point(for: selectedMarker!.position)
            let value = infoWindow!.frame.size.height/2
            let infoWinCenter = CGPoint(x: projection.x, y: projection.y - value - 37.0)
            infoWindow!.center = infoWinCenter
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        if positionMapForInfoWin
        {
            positionMapForInfoWin = false
            insertInfoWindow_()
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Insert Info Window
    //-------------------------------------------------------------------------//
    
    private var positionMapForInfoWin = false
    
    private func insertInfoWindow()
    {
        if let markerId = selectedMarker!.userData as? String
        {
            for business in filteredBusinesses
            {
                if business.getPlaceId() == markerId
                {
                    let infoWin = BusinessInfoWindow.instanceFromNib()
                        infoWin.setUpView(business, placeImgName)
                    
                    let sel = #selector(SearchResults_VC.businessInfoWindowTapped(_:))
                    infoWin.infoWindowButton.addTarget(self, action: sel, for: .touchDown)
                    
                    if infoWindow != nil
                    {
                        infoWindow!.removeFromSuperview()
                    }
                    
                    infoWindow = infoWin
                    
                    break
                }
            }
        }
        else
        {
            let infoWin = MyLocationInfoWindow.instanceFromNib()
                infoWin.setUpView(userAddress)
            
            if infoWindow != nil
            {
                infoWindow!.removeFromSuperview()
            }
            
            infoWindow = infoWin
        }
        
        if mapView.camera.zoom != 16.0
        {
            positionMapForInfoWin = true
            mapView.animate(toZoom: 16.0)
        }
        else
        {
            insertInfoWindow_()
        }
    }
    
    private func insertInfoWindow_()
    {
        let projection = mapView.projection.point(for: selectedMarker!.position)
        let value = infoWindow!.frame.size.height/2
        let infoWinCenter = CGPoint(x: projection.x, y: projection.y - value - 37.0)
        
        let coord = mapView.projection.coordinate(for: infoWinCenter)
        
        let camera = GMSCameraPosition.camera(withLatitude: coord.latitude,
                                              longitude: coord.longitude,
                                              zoom: 16.0)
        
        mapView.animate(to: camera)
        
        infoWindow!.center = infoWinCenter
        view.addSubview(infoWindow!)
        
        view.bringSubview(toFront: backButton)
        view.bringSubview(toFront: labelContainer)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Business Info Window Action
    //-------------------------------------------------------------------------//
    
    @objc func businessInfoWindowTapped(_ button: UIButton)
    {
        let markerId = selectedMarker!.userData as! String
        
        for business in filteredBusinesses
        {
            if business.getPlaceId() == markerId
            {
                let placeDetailsVC = PlaceDetails_VC(nibName: "PlaceDetails_VC", bundle: nil)
                    placeDetailsVC.business = business
                    placeDetailsVC.modalPresentationStyle = .overCurrentContext
                
                present(placeDetailsVC, animated: true, completion: nil)
                
                break
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Get user address
    //-------------------------------------------------------------------------//
    
    private func getUserAddress()
    {
        let location = CLLocation(latitude: Global.userLat, longitude: Global.userLong)
        
        geocoder.reverseGeocodeLocation(location)
        {
            [weak self](placemarks, error) in
            
            if self != nil
            {
                if let placemarks = placemarks, let placemark = placemarks.first
                {
                    let streetAddr = placemark.thoroughfare!
                    let streetAddrComplem = placemark.subThoroughfare!
                    let city = placemark.locality!
                    let state = placemark.administrativeArea!
                    let country = placemark.country!
                    
                    self!.userAddress = "\(streetAddr), \(streetAddrComplem), " +
                                        "\(city), \(state), \(country)"
                    
                    if (self!.selectedMarker != nil)
                    {
                        if self!.selectedMarker!.userData == nil
                        {
                            self!.insertInfoWindow()
                        }
                    }
                }
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
