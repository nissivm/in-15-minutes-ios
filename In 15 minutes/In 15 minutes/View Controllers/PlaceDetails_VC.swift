//
//  PlaceDetails_VC.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 09/06/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import SafariServices
import MapKit

class PlaceDetails_VC: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate, BusinessInfoCellDelegate, GMSMapViewDelegate, ServerRequestDelegate
{
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var containerOne: UIView!
    @IBOutlet weak var enlargeShrinkButton: UIButton!
    
    @IBOutlet weak var containerTwo: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var closeMapContainerButton: UIButton!
    @IBOutlet weak var enlargeShrinkButton2: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var containerTwoWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTwoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTwoRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTwoBottomConstraint: NSLayoutConstraint!
    
    var business: Business!
    
    private var launching = true
    private var center = NotificationCenter.default
    private var serverRequest = ServerRequest()
    private var containerOneIsBig = false
    private var containerTwoIsBig = false
    private var selMarkerUserData: [String : String]?
    private var geometry = ""
    private var markersCoordinates = [CLLocationCoordinate2D]()
    private var instructions = [String]()
    private var bounds: GMSCoordinateBounds?
    private var routePolyline: GMSPolyline?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let sel = #selector(PlaceDetails_VC.screenRotated)
        let name = NSNotification.Name.UIDeviceOrientationDidChange
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        let nib = UINib(nibName: "BusinessInfoCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "BusinessInfoCell")
        
        let camera = GMSCameraPosition.camera(withLatitude: business.getPlaceLatitude(),
                                              longitude: business.getPlaceLongitude(),
                                              zoom: 16.0)
        mapView.camera = camera
        mapView.delegate = self
        serverRequest.delegate = self
    }
    
    override func viewDidLayoutSubviews()
    {
        guard launching else
        {
            return
        }
        
        launching = false
        
        setupStackView() // Called also when screen is rotated
        setupContainerView()
    }
    
    override var prefersStatusBarHidden : Bool
    {
        return true
    }
    
    deinit
    {
        center.removeObserver(self)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Is valid orientation
    //-------------------------------------------------------------------------//
    
    private func isValidOrientation() -> Bool
    {
        let orient = UIDevice.current.orientation
        let isPort = UIDeviceOrientationIsPortrait(orient)
        let isLand = UIDeviceOrientationIsLandscape(orient)
        let isNotUpsideDown = orient != .portraitUpsideDown
        
        return (isPort || isLand) && isNotUpsideDown
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Setup stack view
    //-------------------------------------------------------------------------//
    
    private func setupStackView()
    {
        guard isValidOrientation() else
        {
            return
        }
        
        let screenW = UIScreen.main.bounds.size.width
        let screenH = UIScreen.main.bounds.size.height
        let bigger = screenW > screenH ? screenW : screenH
        let smaller = screenW < screenH ? screenW : screenH
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
        {
            stackView.axis = .vertical
            containerTwoWidthConstraint.constant = smaller
            containerTwoRightConstraint.constant = 0
            
            if containerOneIsBig
            {
                containerTwoHeightConstraint.constant = (bigger/3) * 2
                containerTwoBottomConstraint.constant = ((bigger/3) * 2) * -1
            }
            else if containerTwoIsBig
            {
                containerTwoHeightConstraint.constant = bigger
                containerTwoBottomConstraint.constant = 0
            }
            else
            {
                containerTwoHeightConstraint.constant = (bigger/3) * 2
                containerTwoBottomConstraint.constant = 0
            }
        }
        else
        {
            stackView.axis = .horizontal
            containerTwoHeightConstraint.constant = smaller
            containerTwoBottomConstraint.constant = 0
            
            if containerOneIsBig
            {
                containerTwoWidthConstraint.constant = (bigger/3) * 2
                containerTwoRightConstraint.constant = ((bigger/3) * 2) * -1
            }
            else if containerTwoIsBig
            {
                containerTwoWidthConstraint.constant = bigger
                containerTwoRightConstraint.constant = 0
            }
            else
            {
                containerTwoWidthConstraint.constant = (bigger/3) * 2
                containerTwoRightConstraint.constant = 0
            }
        }
        
        tableView.reloadData()
        view.layoutIfNeeded()
        
        if bounds != nil
        {
            insertRouteAndMarkers()
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Setup container view
    //-------------------------------------------------------------------------//
    
    private func setupContainerView()
    {
        guard isValidOrientation() else
        {
            return
        }
        
        let screenW = UIScreen.main.bounds.size.width
        let screenH = UIScreen.main.bounds.size.height
        let smaller = screenW < screenH ? screenW : screenH
        let bigger = screenW > screenH ? screenW : screenH
        var strViewW = smaller
        var strViewH = bigger/3
        
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
        {
            strViewW = bigger/3
            strViewH = smaller
        }
        
        StreetViewContainerView.placeLat = business.getPlaceLatitude()
        StreetViewContainerView.placeLong = business.getPlaceLongitude()
        
        let strView = StreetViewContainerView(nibName: "StreetViewContainerView", bundle: nil)
        
        addChildViewController(strView)
        strView.view.frame = CGRect(x: 0, y: 0, width: strViewW, height: strViewH)
        containerOne.insertSubview(strView.view, belowSubview: enlargeShrinkButton)
        strView.didMove(toParentViewController: self)
        
        strView.view.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            
            NSLayoutConstraint(item: strView.view, attribute: .left, relatedBy: .equal, toItem: containerOne, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: strView.view, attribute: .right, relatedBy: .equal, toItem: containerOne, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: strView.view, attribute: .top, relatedBy: .equal, toItem: containerOne, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: strView.view, attribute: .bottom, relatedBy: .equal, toItem: containerOne, attribute: .bottom, multiplier: 1, constant: 0)
        ]
        
        containerOne.addConstraints(constraints)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Notifications
    //-------------------------------------------------------------------------//
    
    @objc func screenRotated()
    {
        setupStackView()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func closeButtonTapped(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enlargeShrinkButtonTapped(_ sender: UIButton)
    {
        guard isValidOrientation() else
        {
            return
        }
        
        toggleContainerOne()
    }
    
    @IBAction func closeMapContainerButtonTapped(_ sender: UIButton)
    {
        guard isValidOrientation() else
        {
            return
        }
        
        if containerTwoIsBig
        {
            toggleContainerTwo(closeMap: true)
            return
        }
        
        hideMap()
    }
    
    @IBAction func enlargeShrinkButton2Tapped(_ sender: UIButton)
    {
        guard isValidOrientation() else
        {
            return
        }
        
        toggleContainerTwo(closeMap: false)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: GMSMapViewDelegate
    //-------------------------------------------------------------------------//
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        selMarkerUserData = marker.userData as? [String : String]
        return false // Default behaviour (returns the default info window)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        selMarkerUserData = nil
    }
    
    //-------------------------------------------------------------------------//
    // MARK: ServerRequestDelegate
    //-------------------------------------------------------------------------//
    
    func searchPlacesResult(result: String, filteredBusinesses: [Business]){}
    
    func routeGenerationResult(result: String, geometry: String,
                               markersCoordinates: [CLLocationCoordinate2D],
                               instructions: [String])
    {
        Auxiliar.hideLoadingHUDInView(view)
        
        if result == "Success"
        {
            self.geometry = geometry
            self.markersCoordinates = markersCoordinates
            self.instructions = instructions
            
            showMap()
            insertRouteAndMarkers()
        }
        else
        {
            Auxiliar.presentAlert(title: "Failure", message: result, vc: self)
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Insert Route and Markers
    //-------------------------------------------------------------------------//
    
    private func insertRouteAndMarkers()
    {
        guard markersCoordinates.count > 0 else
        {
            return
        }
        
        mapView.clear()
        bounds = GMSCoordinateBounds()
        var selMarker: GMSMarker?
        
        for (index, coord) in markersCoordinates.enumerated()
        {
            bounds = bounds!.includingCoordinate(coord)
            
            let step = "Step \(index + 1)"
            let instruction = instructions[index]
            let dic: [String : String] = ["step" : step, "instruction" : instruction]
            
            let lat = coord.latitude
            let lng = coord.longitude
            
            let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                marker.userData = dic
                marker.title = step
                marker.snippet = instruction
                marker.icon = GMSMarker.markerImage(with: UIColor(hex: Constants.appBlue))
                marker.appearAnimation = .pop
                marker.map = mapView
            
            if selMarkerUserData != nil
            {
                if selMarkerUserData! == dic
                {
                    selMarker = marker
                    mapView.selectedMarker = marker
                }
            }
        }
        
        if routePolyline == nil
        {
            if let path = GMSPath(fromEncodedPath: geometry)
            {
                routePolyline = GMSPolyline(path: path)
                routePolyline!.strokeColor = UIColor(hex: Constants.appGreen)
                routePolyline!.strokeWidth = 4.0
                routePolyline!.map = mapView
            }
        }
        else
        {
            routePolyline!.map = mapView
        }
        
        guard selMarker == nil else
        {
            let cameraPos = GMSCameraPosition.camera(withLatitude: selMarker!.position.latitude, longitude: selMarker!.position.longitude, zoom: 17)
            
            mapView.animate(to: cameraPos)
            
            return
        }
        
        let corner1 = bounds!.southWest
        let corner2 = bounds!.northEast
        let point1 = MKMapPointForCoordinate(corner1)
        let point2 = MKMapPointForCoordinate(corner2)
        let mapViewWidth = Double(mapView.frame.size.width)
        let mapViewHeight = Double(mapView.frame.size.height)
        let mapScaleWidth = mapViewWidth/fabs(point2.x - point1.x)
        let mapScaleHeight = mapViewHeight/fabs(point2.y - point1.y)
        let mapScale = min(mapScaleWidth, mapScaleHeight)
        
        var zoomBase: Double = 19
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
        {
            zoomBase = 19.5
        }
        
        let centerPoint = MKMapPointMake((point1.x + point2.x) / 2, (point1.y + point2.y) / 2)
        let centerLocation = MKCoordinateForMapPoint(centerPoint)
        
        let zoomLevel = zoomBase + log2(mapScale)
        
        let cameraPos = GMSCameraPosition.camera(withLatitude: centerLocation.latitude, longitude: centerLocation.longitude, zoom: Float(zoomLevel))
        
        mapView.animate(to: cameraPos)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Toggle Container One
    //-------------------------------------------------------------------------//
    
    private func toggleContainerOne()
    {
        var newValue: CGFloat = 0
        let screenW = UIScreen.main.bounds.size.width
        let screenH = UIScreen.main.bounds.size.height
        let bigger = screenW > screenH ? screenW : screenH
        
        if containerOneIsBig
        {
            containerOneIsBig = false
            newValue = 0
        }
        else
        {
            containerOneIsBig = true
            newValue = ((bigger/3) * 2) * -1
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut,
        animations:
        {
            [unowned self]() -> Void in
            
            if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
            {
                self.containerTwoBottomConstraint.constant = newValue
            }
            else
            {
                self.containerTwoRightConstraint.constant = newValue
            }
            
            self.view.layoutIfNeeded()
        },
        completion:
        {
            [unowned self](finished) in
            
            var imgName = ""
            
            if self.containerOneIsBig
            {
                imgName = "ShrinkButton"
            }
            else
            {
                imgName = "EnlargeButton"
                
                self.insertRouteAndMarkers()
            }
            
            let img = UIImage(named: imgName)
            self.enlargeShrinkButton.setBackgroundImage(img, for: UIControlState())
        })
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Toggle Container Two
    //-------------------------------------------------------------------------//
    
    private func toggleContainerTwo(closeMap: Bool)
    {
        var newValue: CGFloat = 0
        let screenW = UIScreen.main.bounds.size.width
        let screenH = UIScreen.main.bounds.size.height
        let bigger = screenW > screenH ? screenW : screenH
        
        if containerTwoIsBig
        {
            containerTwoIsBig = false
            newValue = (bigger/3) * 2
        }
        else
        {
            containerTwoIsBig = true
            newValue = bigger
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut,
        animations:
        {
            if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
            {
                self.containerTwoHeightConstraint.constant = newValue
            }
            else
            {
                self.containerTwoWidthConstraint.constant = newValue
            }
            
            self.view.layoutIfNeeded()
        },
        completion:
        {
            (finished) in
            
            var imgName = ""
            
            if self.containerTwoIsBig
            {
                imgName = "ShrinkButton"
            }
            else
            {
                imgName = "EnlargeButton"
            }
            
            let img = UIImage(named: imgName)
            self.enlargeShrinkButton2.setBackgroundImage(img, for: UIControlState())
            
            self.insertRouteAndMarkers()
            
            if closeMap
            {
                self.hideMap()
            }
        })
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Handle map visibility
    //-------------------------------------------------------------------------//
    
    private func showMap()
    {
        tableView.isHidden = true
        mapView.isHidden = false
        closeMapContainerButton.isHidden = false
        enlargeShrinkButton2.isHidden = false
    }
    
    private func hideMap()
    {
        tableView.isHidden = false
        mapView.isHidden = true
        closeMapContainerButton.isHidden = true
        enlargeShrinkButton2.isHidden = true
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UITableViewDataSource
    //-------------------------------------------------------------------------//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessInfoCell",
                                                 for: indexPath) as! BusinessInfoCell
        
        cell.delegate = self
        cell.setupCellContent(business: business)
        
        return cell
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UITableViewDelegate
    //-------------------------------------------------------------------------//
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let remainingSpace: CGFloat = 305.0 - (25.0 + 18.0)
        
        let placeName = business.getPlaceName()
        let placeNameLabelH = placeName.labelHeightForSelf(maxWidth: Auxiliar.getMaxWidth(), fontSize: 22.0, fontName: "Montserrat-SemiBold")
        
        let placeAddress = business.getPlaceAddr()
        let placeAddressLabelH = placeAddress.labelHeightForSelf(maxWidth: Auxiliar.getMaxWidth(), fontSize: 15.0)
        
        return remainingSpace + (placeNameLabelH + 1.0) + (placeAddressLabelH + 1.0)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: SFSafariViewControllerDelegate
    //-------------------------------------------------------------------------//
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: BusinessInfoCellDelegate
    //-------------------------------------------------------------------------//
    
    func yelpLogoButtonTapped()
    {
        guard let url = URL(string: "https://www.yelp.com") else
        {
            return
        }
        
        let safariVC = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            safariVC.delegate = self
        
        present(safariVC, animated: true, completion: nil)
    }
    
    func routeButtonTapped()
    {
        if geometry != ""
        {
            showMap()
        }
        else
        {
            Auxiliar.showLoadingHUDWithText("Generating route...", view: view)
            
            let lng = business.getPlaceLongitude()
            let lat = business.getPlaceLatitude()
            serverRequest.generateRoute(destLng: lng, destLat: lat)
        }
    }
    
    func placePhoneButtonTapped()
    {
        var formattedNum = business.getPlacePhone().replacingOccurrences(of: "-", with: "")
            formattedNum = formattedNum.replacingOccurrences(of: " ", with: "")
        
        guard let url = URL(string: "telprompt://" + formattedNum) else
        {
            return
        }
        
        guard UIApplication.shared.canOpenURL(url) else
        {
            return
        }
        
        if #available(iOS 10.0, *)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        else
        {
            UIApplication.shared.openURL(url)
        }
    }
    
    func websiteButtonTapped()
    {
        guard let url = URL(string: business.getPlaceUrl()) else
        {
            return
        }
        
        let safariVC = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            safariVC.delegate = self
        
        present(safariVC, animated: true, completion: nil)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
