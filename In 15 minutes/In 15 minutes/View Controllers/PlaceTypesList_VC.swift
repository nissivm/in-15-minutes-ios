//
//  PlaceTypesList_VC.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 06/06/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import CoreLocation

class PlaceTypesList_VC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, ServerRequestDelegate
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var locationPermissionView: UIView!
    @IBOutlet weak var splashScreenView: UIView!
    @IBOutlet weak var subviewOne: UIView!
    @IBOutlet weak var subviewTwo: UIView!
    @IBOutlet weak var permissionLocationInfoLabel: UILabel!
    @IBOutlet weak var allowLocationButton: UIButton!
    
    private var launching = true
    private var launching_2 = true
    private var center = NotificationCenter.default
    private let locationManager = CLLocationManager()
    private var justUpdatedLocation = false
    private var userLocationUpdated = false
    private var fetchingPlaces = false
    
    private var serverRequest = ServerRequest()
    
    private var placeName = ""
    private var placeCategory = ""
    private var placeImgName = ""
    
    private var searchResults = [String : [Business]]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        serverRequest.delegate = self
        
        var sel = #selector(PlaceTypesList_VC.appWillEnterForeground)
        var name = NSNotification.Name.UIApplicationWillEnterForeground
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(PlaceTypesList_VC.screenRotated)
        name = NSNotification.Name.UIDeviceOrientationDidChange
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        collectionView.register(UINib(nibName: "PlaceTypeCell", bundle: nil),
                                forCellWithReuseIdentifier: "PlaceTypeCell")
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // To avoid the upper blank space:
        
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews()
    {
        guard launching else
        {
            return
        }
        
        launching = false
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            startUpdatingLocation()
        }
        
        setupCollectionForOrientation()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        serverRequest.delegate = self
        
        guard launching_2 else
        {
            return
        }
        
        launching_2 = false
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse
        {
            performAnimation()
        }
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    deinit
    {
        center.removeObserver(self)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Notifications
    //-------------------------------------------------------------------------//
    
    @objc func appWillEnterForeground()
    {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            justUpdatedLocation = false
            startUpdatingLocation()
        }
        else
        {
            fetchingPlaces = false
            Auxiliar.hideLoadingHUDInView(view)
            userLocationUpdated = false
        }
    }
    
    @objc func screenRotated()
    {
        setupCollectionForOrientation()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func allowLocationButtonTapped(_ sender: UIButton)
    {
        if CLLocationManager.authorizationStatus() == .notDetermined
        {
            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            takeMeThereCancelDialog()
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Setup collection for orientation
    //-------------------------------------------------------------------------//
    
    private func setupCollectionForOrientation()
    {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
        {
            collectionView.showsVerticalScrollIndicator = false
            collectionView.showsHorizontalScrollIndicator = true
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                layout.scrollDirection = UICollectionViewScrollDirection.horizontal
            collectionView.collectionViewLayout = layout
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
        {
            collectionView.showsVerticalScrollIndicator = true
            collectionView.showsHorizontalScrollIndicator = false
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                layout.scrollDirection = UICollectionViewScrollDirection.vertical
            collectionView.collectionViewLayout = layout
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Start updating location
    //-------------------------------------------------------------------------//
    
    private func startUpdatingLocation()
    {
        background.isHidden = true
        locationPermissionView.isHidden = true
        locationPermissionView.alpha = 1
        splashScreenView.isHidden = true
        locationManager.startUpdatingLocation()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Perform animation
    //-------------------------------------------------------------------------//
    
    private func performAnimation()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut,
        animations:
        {
            [unowned self]() -> Void in
            
            self.locationPermissionView.alpha = 1
            self.splashScreenView.alpha = 0
            self.view.layoutIfNeeded()
        },
        completion:
        {
            [unowned self](finished) in
                
            self.splashScreenView.isHidden = true
        })
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Take me there / Cancel dialog
    //-------------------------------------------------------------------------//
    
    private func takeMeThereCancelDialog()
    {
        let title = "Location tracking not authorized"
        let msg = "1. Go to your Settings\n" +
                  "2. Scroll down and tap on Privacy\n" +
                  "3. Select Location Services\n" +
                  "4. Turn Location Services On\n" +
                  "5. Find In 15 minutes, tap on it and select While Using the App"
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Take me there!", style: .default) {
            
            (action) in
            
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString)
            {
                UIApplication.shared.openURL(appSettings)
            }
        }
        
        alert.addAction(action)
        
        let action_ = UIAlertAction(title: "Cancel", style: .default)
        {
            [unowned self](action) in
            
            self.fetchingPlaces = false
            Auxiliar.hideLoadingHUDInView(self.view)
        }
        
        alert.addAction(action_)
        
        present(alert, animated: true, completion: nil)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: CLLocationManagerDelegate
    //-------------------------------------------------------------------------//
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if status == .authorizedWhenInUse
        {
            startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if justUpdatedLocation { return }
        
        print("\n didUpdateLocations \n")
        
        justUpdatedLocation = true
        userLocationUpdated = true
        
        let location = locations.last!
        
        locationManager.stopUpdatingLocation()
        
        Global.userLat = location.coordinate.latitude
        Global.userLong = location.coordinate.longitude
        
        // Location in Lisbon, Portugal:
        //Global.userLat = 38.727186
        //Global.userLong = -9.143785
        
        // Location near Main street (Vancouver, BC, Canada):
        //Global.userLat = 49.248975
        //Global.userLong = -123.094981
        
        if searchResults.count > 0
        {
            searchResults.removeAll()
        }
        
        if fetchingPlaces
        {
            serverRequest.fetchPlaces(placeCategory)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        if justUpdatedLocation { return }
        
        print("\n didFailWithError: \(error.localizedDescription) \n")
        
        justUpdatedLocation = true
        userLocationUpdated = false
        
        locationManager.stopUpdatingLocation()
        
        if fetchingPlaces
        {
            if Global.userLat != 0
            {
                serverRequest.fetchPlaces(placeCategory)
            }
            else
            {
                fetchingPlaces = false
                Auxiliar.hideLoadingHUDInView(view)
                Auxiliar.presentAlert(title: "Error",
                                      message: "An error occurred, please try again.",
                                      vc: self)
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: ServerRequestDelegate
    //-------------------------------------------------------------------------//
    
    func searchPlacesResult(result: String, filteredBusinesses: [Business])
    {
        fetchingPlaces = false
        Auxiliar.hideLoadingHUDInView(view)
        
        if result == "Success"
        {
            searchResults[placeName] = filteredBusinesses
            
            if AppDelegate.appIsBackgrounded
            {
                return
            }
            
            showPlacesSearchResults(filteredBusinesses)
        }
        else
        {
            Auxiliar.presentAlert(title: "Failure", message: result, vc: self)
        }
    }
    
    func routeGenerationResult(result: String, geometry: String,
                               markersCoordinates: [CLLocationCoordinate2D],
                               instructions: [String]){}
    
    //-------------------------------------------------------------------------//
    // MARK: Show places search results
    //-------------------------------------------------------------------------//
    
    private func showPlacesSearchResults(_ filteredBusinesses: [Business])
    {
        let searchResultsVC = SearchResults_VC(nibName: "SearchResults_VC", bundle: nil)
            searchResultsVC.placeName = placeName
            searchResultsVC.placeImgName = placeImgName
            searchResultsVC.filteredBusinesses = filteredBusinesses
        
        navigationController?.pushViewController(searchResultsVC, animated: true)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDataSource
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return Constants.placeImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceTypeCell",
                                                      for: indexPath) as! PlaceTypeCell
        
        var imageName = Constants.placeImagesArray[indexPath.item]
        
        if Device.IS_IPHONE_4 || Device.IS_IPHONE_5
        {
            imageName += ".jpg"
        }
        else if Device.IS_IPHONE_6_OR_NEWER
        {
            imageName += "_6.jpg"
        }
        else
        {
            imageName += "_6p.jpg"
        }
        
        cell.placeImage.image = UIImage(named: imageName)
        cell.placeTypeName.text = Constants.placesNamesArray[indexPath.item]
        
        return cell
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDelegateFlowLayout
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let viewWidth = view.frame.size.width
        let viewHeight = view.frame.size.height
        let side = viewWidth < viewHeight ? viewWidth : viewHeight
        
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UITableViewDelegate
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        placeName = Constants.placesNamesArray[indexPath.item]
        placeCategory = Constants.placeCategoriesArray[indexPath.item]
        placeImgName = Constants.placeImagesArray[indexPath.item]
        
        if searchResults.count > 0
        {
            if let businessArr = searchResults[placeName]
            {
                showPlacesSearchResults(businessArr)
                return
            }
        }
        
        fetchingPlaces = true
        Auxiliar.showLoadingHUDWithText("Searching places...", view: view)
        
        if userLocationUpdated
        {
            serverRequest.fetchPlaces(placeCategory)
            
            return
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            justUpdatedLocation = false
            startUpdatingLocation()
        }
        else
        {
            takeMeThereCancelDialog()
        }
        
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
