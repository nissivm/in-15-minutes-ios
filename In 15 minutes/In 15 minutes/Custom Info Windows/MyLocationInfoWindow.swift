//
//  MyLocationInfoWindow.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/15/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class MyLocationInfoWindow: UIView
{
    @IBOutlet weak var myLocationInfoContainer: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var myLocationInfoContainerHeightConstraint: NSLayoutConstraint!
    
    class func instanceFromNib() -> MyLocationInfoWindow
    {
        let nib = UINib(nibName: "MyLocationInfoWindow", bundle: nil)
        return nib.instantiate(withOwner: nil, options: nil)[0] as! MyLocationInfoWindow
    }
    
    //-------------------------------------------------------------------------//
    // MARK: View Set Up
    //-------------------------------------------------------------------------//
    
    func setUpView(_ userAddress: String)
    {
        if userAddress == ""
        {
            frame = CGRect(x: 0, y: 0, width: 300.0, height: 85.0)
        }
        else
        {
            let maxWidth: CGFloat = 280.0
            let oneLineHeight: CGFloat = 18.0
            let remainingHeight = 70.0 - oneLineHeight
            
            let addressHeight = userAddress.labelHeightForSelf(maxWidth: maxWidth, fontSize: 15.0)
            
            let viewHeight = remainingHeight + addressHeight
            myLocationInfoContainerHeightConstraint.constant = viewHeight
            
            frame = CGRect(x: 0, y: 0, width: 300.0, height: viewHeight + 15.0)
            
            addressLabel.text = userAddress
        }
        
        myLocationInfoContainer.layer.borderColor = UIColor(hex: Constants.appLightGray).cgColor
        myLocationInfoContainer.layer.borderWidth = 0.5
    }
}
