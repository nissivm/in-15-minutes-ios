//
//  BusinessInfoWindow.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/16/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class BusinessInfoWindow: UIView
{
    @IBOutlet weak var businessInfoContainer: UIView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeImageView: UIImageView!
    
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var infoWindowButton: UIButton!
    
    class func instanceFromNib() -> BusinessInfoWindow
    {
        let nib = UINib(nibName: "BusinessInfoWindow", bundle: nil)
        return nib.instantiate(withOwner: nil, options: nil)[0] as! BusinessInfoWindow
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Setup view
    //-------------------------------------------------------------------------//
    
    func setUpView(_ business: Business, _ placeImgName: String)
    {
        frame = CGRect(x: 0, y: 0, width: 300.0, height: 165.0)
        
        let placeImageUrl = business.getPlaceImageUrl()
        let placeName = business.getPlaceName()
        let rating = business.getPlaceRating()
        let reviewCount = business.getPlaceReviewCount()
        let distanceInSeconds = business.getPlaceDistance()
        let distanceInMinutes = distanceInSeconds/60
        
        let path = Bundle.main.path(forResource: placeImgName, ofType: "jpg")!
        let img = UIImage(contentsOfFile: path)!
        let desiredSize = CGSize(width: placeImageView.frame.size.width,
                                 height: placeImageView.frame.size.height)
        
        placeImageView.image = UIImage.resizeImage(img, desiredSize: desiredSize)
        
        placeNameLabel.text = placeName
        ratingImageView.image = Auxiliar.getRatingImage(rating: rating)
        reviewCountLabel.text = Auxiliar.getReviewText(reviewCount: reviewCount)
        distanceLabel.text = "\(Int(ceil(distanceInMinutes))) min away"
        
        businessInfoContainer.layer.borderColor = UIColor(hex: Constants.appLightGray).cgColor
        businessInfoContainer.layer.borderWidth = 0.5
        
        guard placeImageUrl != "" else
        {
            return
        }
        
        if let manager = SDWebImageManager.shared()
        {
            if let url = URL(string: placeImageUrl)
            {
                manager.downloadImage(with: url, options: .avoidAutoSetImage,
                                      progress: nil,
                                      completed:
                {
                    [weak self](image, error, cacheType, finished, imageURL) -> Void in
                    
                    if self != nil
                    {
                        if let img = image
                        {
                            let desiredSize = CGSize(width: self!.placeImageView.frame.size.width,
                                                     height: self!.placeImageView.frame.size.height)
                            
                            self!.placeImageView.image = UIImage.resizeImage(img, desiredSize: desiredSize)
                        }
                    }
                })
            }
        }
    }
}
