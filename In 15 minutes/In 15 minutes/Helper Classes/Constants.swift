//
//  Constants.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/10/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class Constants
{
    // Multipliers
    static let multiplier6: CGFloat = 1.14
    static let multiplier6plus: CGFloat = 1.28
    
    // Colors
    static let appGreen = "03c174"
    static let appBlue = "4eb7f6"
    static let appGray = "262B2E"
    static let appLightGray = "b1b3b4"
    
    // Places arrays:
    
    static let placeImagesArray = ["restaurants", "cafes", "bakeries", "bars",
                                    "supermarkets", "bookstores", "shopping_malls",
                                    "home_goods", "pet_stores", "museums", "theaters",
                                    "movie_theaters", "churches", "parks", "bus_station",
                                    "subway_station", "beauty_salon", "gym", "pharmacies",
                                    "hospitals"]
    
    static let placesNamesArray = ["Restaurants", "Cafes", "Bakeries", "Bars",
                                    "Supermarkets", "Bookstores", "Shopping",
                                    "Home goods", "Pets", "Museums", "Theaters",
                                    "Movie theaters", "Churches", "Parks, Plazas & Zoos",
                                    "Bus stations", "Subway stations", "Beauty salons",
                                    "Gym", "Pharmacies", "Hospitals"]
    
    static let placeCategoriesArray = [
        "foodtrucks,restaurants,kiosk,dancerestaurants",
        "acaibowls,bubbletea,churros,coffee,convenience,cupcakes,delicatessen,empanadas,friterie,gelato,icecream,internetcafe,milkshakebars,cakeshop,pretzels,tea,tortillas,coffeeshops",
        "bakeries,bagels,donuts",
        "barcrawl,bars,beergardens,jazzandblues,pianobars,poolhalls",
        "butcher,ethicgrocery,farmersmarket,fishmonger,grocery,intlgrocery,organic_stores,markets,seafoodmarkets",
        "bookstores,comicbooks,musicvideo,mags,usedbooks",
        "childcloth,deptstores,hats,lingerie,maternity,menscloth,plus_size_fashion,shoes,sleepwear,sportswear,swimwear,vintage,womenscloth,shoppingcenters,thrift_stores,watches,wigs",
        "furniture,homedecor,mattresses,gardening,outdoorfurniture,paintstores,tableware",
        "emergencypethospital,groomer,vet",
        "galleries,museums,planetarium",
        "opera,theater",
        "movietheaters",
        "churches,synagogues",
        "gardens,parks,playgrounds,publicplazas,zoos",
        "busstations",
        "metrostations",
        "barbers,eyebrowservices,hair_extensions,waxing,hair,makeupartists,othersalons",
        "cardioclasses,dancestudio,gyms,pilates,swimminglessons,taichi,yoga,gymnastics",
        "drugstores,herbalshops,pharmacy,vitaminssupplements",
        "emergencyrooms,hospitals,medcenters,urgent_care"
    ]
}
