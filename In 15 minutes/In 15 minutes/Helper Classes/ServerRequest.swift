//
//  ServerRequest.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/13/17.
//  Copyright � 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

protocol ServerRequestDelegate: class
{
    func searchPlacesResult(result: String, filteredBusinesses: [Business])
    func routeGenerationResult(result: String, geometry: String,
                               markersCoordinates: [CLLocationCoordinate2D],
                               instructions: [String])
}

class ServerRequest
{
    static private let YELP_API_KEY = "Your key"
    static private let MAPBOX_TOKEN = "Your token"
    
    private var alreadyGettingToken = false
    private var businesses = [Business]()
    private var filteredBusinesses = [Business]()
    
    weak var delegate: ServerRequestDelegate?
    
    //-------------------------------------------------------------------------//
    // MARK: Fetch places
    //-------------------------------------------------------------------------//
    
    func fetchPlaces(_ placeCategory: String)
    {
        guard Reachability.connectedToNetwork() else
        {
            delegate!.searchPlacesResult(result: "No internet connection", filteredBusinesses: [Business]())
            return
        }
        
        var urlStr = "https://api.yelp.com/v3/businesses/search?"
            urlStr += "latitude=\(Global.userLat)&longitude=\(Global.userLong)"
            urlStr += "&categories=" + placeCategory + "&locale=en_US"
            urlStr += "&limit=\(50)&sort_by=distance"
        
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral)
        let request = NSMutableURLRequest(url: URL(string: urlStr)!)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer " + ServerRequest.YELP_API_KEY, forHTTPHeaderField: "Authorization")
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: {
            
            (data, reponse, error) in
            
            DispatchQueue.main.async
            {
                let errorMessage = "An error occurred, please try again."
                
                guard error == nil else
                {
                    print("Error (fetchPlaces): \(String(describing: error))")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                guard data != nil else
                {
                    print("fetchPlaces -> data == nil")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                var jSon: [String : Any]!
                
                do
                {
                    jSon = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : Any]
                }
                catch
                {
                    print("fetchPlaces -> Error while deserializing Json")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                let businessesArray = jSon["businesses"] as! [[String : Any]]
                
                if businessesArray.count > 0
                {
                    if self.businesses.count > 0
                    {
                        self.businesses.removeAll()
                    }
                    
                    if self.filteredBusinesses.count > 0
                    {
                        self.filteredBusinesses.removeAll()
                    }
                    
                    self.createBusinessObjects(businessesArray);
                }
                else
                {
                    self.delegate!.searchPlacesResult(result: "Search returned no results for your surroundings.", filteredBusinesses: [Business]())
                }
            }
        })
        
        dataTask.resume()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Create Business objects
    //-------------------------------------------------------------------------//
    
    private func createBusinessObjects(_ businessesArray: [[String : Any]])
    {
        for businessDic in businessesArray
        {
            let business = Business(businessDic)
            
            if !business.isPlaceClosed()
            {
                if (business.getPlaceName() != "") && (business.getPlaceAddr() != "") &&
                   (business.getPlaceLatitude() != 0) && (business.getPlaceLongitude() != 0)
                {
                    businesses.append(business)
                }
            }
        }
        
        if businesses.count > 0
        {
            fetchDistances()
        }
        else
        {
            delegate!.searchPlacesResult(result: "Search returned no results for your surroundings.", filteredBusinesses: [Business]())
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Fetch distances
    //-------------------------------------------------------------------------//
    
    private func fetchDistances()
    {
        let maxDuration: Double = 900 // Equivalent in seconds of 15 minutes
        var coordinates = "\(Global.userLong),\(Global.userLat);"
        var destinations = "destinations="
        var counter = 0
        
        for business in businesses
        {
            let placeLong: Double = business.getPlaceLongitude()
            let placeLat: Double = business.getPlaceLatitude()
            
            coordinates += "\(placeLong),\(placeLat)"
            destinations += "\((counter + 1))"
            
            if (counter < (businesses.count - 1)) && (counter < 23)
            {
                coordinates += ";"
                destinations += ";"
            }
            
            if counter < 23
            {
                counter += 1
            }
            else
            {
                break
            }
        }
        
        let urlStr = "https://api.mapbox.com/directions-matrix/v1/mapbox/walking/" + coordinates +
                     "?sources=0&" + destinations + "&access_token=" + ServerRequest.MAPBOX_TOKEN
        
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral)
        let url = URL(string: urlStr)!
        
        let dataTask = session.dataTask(with: url, completionHandler: {
        
            (data, reponse, error) in
            
            DispatchQueue.main.async
            {
                let errorMessage = "An error occurred, please try again."
                
                guard error == nil else
                {
                    print("Error (fetchDistances): \(String(describing: error))")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                guard data != nil else
                {
                    print("fetchDistances -> data == nil")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                var jSon: [String : Any]!
                
                do
                {
                    jSon = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : Any]
                }
                catch
                {
                    print("fetchDistances -> Error while deserializing Json")
                    
                    self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    return
                }
                
                let arr = jSon["durations"] as! [Any]
                let durationsArr = arr[0] as! [Any?]
                
                if durationsArr.count == 0
                {
                    if self.filteredBusinesses.count > 0
                    {
                        self.delegate!.searchPlacesResult(result: "Success", filteredBusinesses: self.filteredBusinesses)
                    }
                    else
                    {
                        self.delegate!.searchPlacesResult(result: errorMessage, filteredBusinesses: [Business]())
                    }
                    
                    return
                }
                
                for item in durationsArr
                {
                    if item != nil
                    {
                        let duration: Double = item as! Double
                        
                        if duration <= maxDuration
                        {
                            let business = self.businesses[0]
                                business.setDistance(duration)
                            
                            self.filteredBusinesses.append(business)
                        }
                    }
                    
                    self.businesses.remove(at: 0)
                }
                
                if self.businesses.count > 0
                {
                    self.fetchDistances()
                }
                else if self.filteredBusinesses.count > 0
                {
                    self.delegate!.searchPlacesResult(result: "Success", filteredBusinesses: self.filteredBusinesses)
                }
                else
                {
                    self.delegate!.searchPlacesResult(result: "Search returned no results for your surroundings.",
                                   filteredBusinesses: [Business]())
                }
            }
        })
        
        dataTask.resume()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Generate route
    //-------------------------------------------------------------------------//
    
    func generateRoute(destLng: Double, destLat: Double)
    {
        guard Reachability.connectedToNetwork() else
        {
            delegate!.routeGenerationResult(result: "No internet connection", geometry: "",
                      markersCoordinates: [CLLocationCoordinate2D](), instructions: [String]())
            return
        }
        
        let urlStr = "https://api.mapbox.com/directions/v5/mapbox/walking/\(Global.userLong),\(Global.userLat);" +
                     "\(destLng),\(destLat)?steps=true&access_token=" + ServerRequest.MAPBOX_TOKEN
        
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral)
        let url = URL(string: urlStr)!
        
        let dataTask = session.dataTask(with: url, completionHandler: {
            
            (data, reponse, error) in
            
            DispatchQueue.main.async
            {
                if self.delegate == nil
                {
                    return
                }
                
                let errorMessage = "An error occurred, please try again."
                
                guard error == nil else
                {
                    print("Error (generateRoute): \(String(describing: error))")
                    
                    self.delegate!.routeGenerationResult(result: errorMessage, geometry: "",
                                   markersCoordinates: [CLLocationCoordinate2D](), instructions: [String]())
                    return
                }
                
                guard data != nil else
                {
                    print("generateRoute -> data == nil")
                    
                    self.delegate!.routeGenerationResult(result: errorMessage, geometry: "",
                                   markersCoordinates: [CLLocationCoordinate2D](), instructions: [String]())
                    return
                }
                
                var jSon: [String : Any]!
                
                do
                {
                    jSon = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : Any]
                }
                catch
                {
                    print("generateRoute -> Error while deserializing Json")
                    
                    self.delegate!.routeGenerationResult(result: errorMessage, geometry: "",
                                   markersCoordinates: [CLLocationCoordinate2D](), instructions: [String]())
                    return
                }
                
                let routes = jSon["routes"] as! [Any]
                let routesFirstItem = routes[0] as! [String : Any]
                let geometry = routesFirstItem["geometry"] as! String
                
                let legs = routesFirstItem["legs"] as! [Any]
                let legsFirstItem = legs[0] as! [String : Any]
                let steps = legsFirstItem["steps"] as! [Any]
                
                var markersCoordinates = [CLLocationCoordinate2D]()
                var instructions = [String]()
                
                for step in steps
                {
                    let maneuver = (step as! [String : Any])["maneuver"] as! [String : Any]
                    
                    let location = maneuver["location"] as! [Any]
                    let lng: Double = location[0] as! Double
                    let lat: Double = location[1] as! Double
                    
                    let coord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    markersCoordinates.append(coord)
                    
                    let instruction = maneuver["instruction"] as! String
                    instructions.append(instruction)
                }
                
                self.delegate!.routeGenerationResult(result: "Success", geometry: geometry,
                               markersCoordinates: markersCoordinates, instructions: instructions)
            }
        })
        
        dataTask.resume()
    }
}
