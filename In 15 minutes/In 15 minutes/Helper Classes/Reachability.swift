//
//  Reachability.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 2/4/16.
//  Copyright © 2016 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import SystemConfiguration

open class Reachability
{
    class func connectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress,
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                                                                    
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        })
        else
        {
            return false
        }
        
        var flags : SCNetworkReachabilityFlags = []
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags)
        {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}
