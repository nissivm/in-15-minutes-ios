//
//  Business.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 4/12/17.
//  Copyright © 2017 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class Business
{
    private var businessDic: [String : Any]!
    
    private var placeId = ""
    private var placeName = ""
    private var placeAddr = ""
    private var placePhone = ""
    private var placePriceLevel = ""
    private var placeRating: Double = 0
    private var placeReviewCount = 0
    private var placeImageUrl = ""
    private var placeUrl = ""
    private var placeClosed = true
    private var placeDistance: Double = 0
    private var placeLatitude: Double = 0
    private var placeLongitude: Double = 0
    
    init(_ businessDic: [String : Any])
    {
        self.businessDic = businessDic
        setProperties()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Setters
    //-------------------------------------------------------------------------//
    
    private func setProperties()
    {
        placeId = UUID().uuidString
        
        if let name = businessDic["name"] as? String
        {
            placeName = name
        }
        
        if let loc = businessDic["location"] as? [String : Any]
        {
            if let displayAddr = loc["display_address"] as? [String]
            {
                if displayAddr.count > 0
                {
                    var addr = ""
                    var counter = 0
                    
                    for addrPiece in displayAddr
                    {
                        addr += addrPiece
                        
                        if counter < (displayAddr.count - 1)
                        {
                            addr += ", ";
                        }
                        
                        counter += 1
                    }
                    
                    placeAddr = addr
                }
            }
        }
        
        if let phone = businessDic["display_phone"] as? String
        {
            placePhone = phone
        }
        
        if let price = businessDic["price"] as? String
        {
            placePriceLevel = price
        }
        
        if let r = businessDic["rating"] as? Double
        {
            placeRating = r
        }
        
        if let review = businessDic["review_count"] as? Int
        {
            placeReviewCount = review
        }
        
        if let imgUrl = businessDic["image_url"] as? String
        {
            placeImageUrl = imgUrl
            placeImageUrl = placeImageUrl.replacingOccurrences(of: "\\", with: "")
        }
        
        if let u = businessDic["url"] as? String
        {
            placeUrl = u
            placeUrl = placeUrl.replacingOccurrences(of: "\\", with: "")
        }
        
        if let closed = businessDic["is_closed"] as? Bool
        {
            placeClosed = closed
        }
        
        if let coordinates = businessDic["coordinates"] as? [String : Any]
        {
            if let lat = coordinates["latitude"] as? Double
            {
                placeLatitude = lat
            }
            
            if let lng = coordinates["longitude"] as? Double
            {
                placeLongitude = lng
            }
        }
    }
    
    func setDistance(_ value: Double)
    {
        placeDistance = value
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Getters
    //-------------------------------------------------------------------------//
    
    func getPlaceId() -> String
    {
        return placeId
    }
    
    func getPlaceName() -> String
    {
        return placeName
    }
    
    func getPlaceAddr() -> String
    {
        return placeAddr
    }
    
    func getPlacePhone() -> String
    {
        return placePhone
    }
    
    func getPlacePriceLevel() -> String
    {
        return placePriceLevel
    }
    
    func getPlaceRating() -> Double
    {
        return placeRating
    }
    
    func getPlaceReviewCount() -> Int
    {
        return placeReviewCount
    }
    
    func getPlaceImageUrl() -> String
    {
        return placeImageUrl
    }
    
    func getPlaceUrl() -> String
    {
        return placeUrl
    }
    
    func isPlaceClosed() -> Bool
    {
        return placeClosed
    }
    
    func getPlaceDistance() -> Double
    {
        return placeDistance
    }
    
    func getPlaceLatitude() -> Double
    {
        return placeLatitude
    }
    
    func getPlaceLongitude() -> Double
    {
        return placeLongitude
    }
}
