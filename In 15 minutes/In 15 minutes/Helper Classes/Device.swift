//
//  Device.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 2/4/16.
//  Copyright © 2016 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class Device
{
    static var IS_IPHONE: Bool {
        get {
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                return true
            }
            else
            {
                return false
            }
        }
    }
    
    static var IS_IPHONE_4: Bool
    {
        get
        {
            return IS_IPHONE && (UIScreen.main.nativeBounds.height == 960.0)
        }
    }
    
    static var IS_IPHONE_5: Bool
    {
        get
        {
            return IS_IPHONE && (UIScreen.main.nativeBounds.height == 1136.0)
        }
    }
    
    static var IS_IPHONE_6_OR_NEWER: Bool
    {
        get
        {
            return IS_IPHONE && (UIScreen.main.nativeBounds.height == 1334.0)
        }
    }
    
    static var IS_IPHONE_6_PLUS_OR_NEWER: Bool
    {
        get
        {
            return IS_IPHONE && ((UIScreen.main.nativeBounds.height == 1920.0) || (UIScreen.main.nativeBounds.height == 2208.0))
        }
    }
    
    static var IS_IPHONE_X: Bool
    {
        get
        {
            return IS_IPHONE && (UIScreen.main.nativeBounds.height == 2436.0)
        }
    }
}
