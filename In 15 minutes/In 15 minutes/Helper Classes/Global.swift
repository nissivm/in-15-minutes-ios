//
//  Global.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 10/24/15.
//  Copyright © 2015 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class Global
{
    static var userLat: Double = 0.0
    static var userLong: Double = 0.0
}
