//
//  Auxiliar.swift
//  In 15 minutes
//
//  Created by Nissi Vieira Miranda on 06/06/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class Auxiliar
{
    //-------------------------------------------------------------------------//
    // MARK: MBProgressHUD
    //-------------------------------------------------------------------------//
    
    static private var progressHud: MBProgressHUD?
    
    static func showLoadingHUDWithText(_ text: String, view: UIView)
    {
        if progressHud == nil
        {
            progressHud = MBProgressHUD.showAdded(to: view, animated: true)
        }
        
        progressHud!.labelText = text
    }
    
    static func hideLoadingHUDInView(_ view: UIView)
    {
        if progressHud != nil
        {
            MBProgressHUD.hideAllHUDs(for: view, animated: true)
            progressHud = nil
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Retrieve multiplier
    //-------------------------------------------------------------------------//
    
//    static func retrieveMultiplier() -> CGFloat
//    {
//        var multiplier: CGFloat = 1.0
//
//        if Device.IS_IPHONE_6
//        {
//            multiplier = Constants.multiplier6
//        }
//
//        if Device.IS_IPHONE_6_PLUS
//        {
//            multiplier = Constants.multiplier6plus
//        }
//
//        return multiplier
//    }
    
    //-------------------------------------------------------------------------//
    // MARK: Get max width
    //-------------------------------------------------------------------------//
    
    static func getMaxWidth() -> CGFloat
    {
        //let multiplier = retrieveMultiplier()
        let screenW = UIScreen.main.bounds.size.width
        return screenW - (60.0 + 15.0)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Get background image
    //-------------------------------------------------------------------------//
    
//    static func getBackgroundImage() -> UIImage
//    {
//        var imageName = "background"
//
//        if Device.IS_IPHONE_6_PLUS
//        {
//            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
//            {
//                imageName += "_6plus_land.jpg"
//            }
//            else // UIDeviceOrientationIsPortrait(UIDevice.current.orientation) == true
//            {
//                imageName += "_6plus.jpg"
//            }
//        }
//        else if Device.IS_IPHONE_6
//        {
//            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
//            {
//                imageName += "_6_land.jpg"
//            }
//            else // UIDeviceOrientationIsPortrait(UIDevice.current.orientation) == true
//            {
//                imageName += "_6.jpg"
//            }
//        }
//        else if Device.IS_IPHONE_5
//        {
//            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
//            {
//                imageName += "_5_land.jpg"
//            }
//            else // UIDeviceOrientationIsPortrait(UIDevice.current.orientation) == true
//            {
//                imageName += "_5.jpg"
//            }
//        }
//        else
//        {
//            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
//            {
//                imageName += "_4_land.jpg"
//            }
//            else // UIDeviceOrientationIsPortrait(UIDevice.current.orientation) == true
//            {
//                imageName += "_4.jpg"
//            }
//        }
//
//        return UIImage(named: imageName)!
//    }
    
    //-------------------------------------------------------------------------//
    // MARK: Get splash screen image
    //-------------------------------------------------------------------------//
    
//    static func getSplashScreenImage() -> UIImage
//    {
//        var imageName = "splash_screen"
//
//        if Device.IS_IPHONE_6_PLUS
//        {
//            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
//            {
//                imageName += "_6p_land"
//            }
//            else // UIDeviceOrientationIsPortrait(UIDevice.current.orientation) == true
//            {
//                imageName += "_6p_port"
//            }
//        }
//        else if Device.IS_IPHONE_6
//        {
//            imageName += "_6"
//        }
//        else if Device.IS_IPHONE_5
//        {
//            imageName += "_5"
//        }
//        else
//        {
//            imageName += "_4"
//        }
//
//        return UIImage(named: imageName)!
//    }
    
    //-------------------------------------------------------------------------//
    // MARK: Alert Controller
    //-------------------------------------------------------------------------//
    
    static func presentAlert(title: String, message: String, vc: UIViewController)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        vc.present(alert, animated: true, completion: nil)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Getting rate image
    //-------------------------------------------------------------------------//
    
    static func getRatingImage(rating: Double) -> UIImage
    {
        var imageName = ""
        
        if rating < 1
        {
            imageName = "stars_0"
        }
        else if rating == 1
        {
            imageName = "stars_1"
        }
        else if (rating > 1) && (rating <= 1.5)
        {
            imageName = "stars_1_half"
        }
        else if (rating > 1.5) && (rating <= 2)
        {
            imageName = "stars_2"
        }
        else if (rating > 2) && (rating <= 2.5)
        {
            imageName = "stars_2_half"
        }
        else if (rating > 2.5) && (rating <= 3)
        {
            imageName = "stars_3"
        }
        else if (rating > 3) && (rating <= 3.5)
        {
            imageName = "stars_3_half"
        }
        else if (rating > 3.5) && (rating <= 4)
        {
            imageName = "stars_4"
        }
        else if (rating > 4) && (rating <= 4.5)
        {
            imageName = "stars_4_half"
        }
        else
        {
            imageName = "stars_5"
        }
        
        let path = Bundle.main.path(forResource: imageName, ofType: "png")!
        return UIImage(contentsOfFile: path)!
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Get review text
    //-------------------------------------------------------------------------//
    
    static func getReviewText(reviewCount: Int) -> String
    {
        var reviewText = "\(reviewCount) Reviews"
    
        if reviewCount == 1
        {
            reviewText = "\(reviewCount) Review"
        }
    
        return reviewText
    }
}
